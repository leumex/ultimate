<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>Internationalization</title>
</head>
<body>
<jsp:useBean id="bundleProvider" class="by.minsk.training.localization.BundleProvider"/>
<fmt:setLocale value="${bundleProvider.getLocale(language)}"/>
<fmt:setBundle basename="resource" var="bndle"/>
</body>

