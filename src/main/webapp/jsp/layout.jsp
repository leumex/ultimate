<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/style.css"/>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/create_tender.js"></script>
    <title>TMS 2020(c)</title>
</head>
<body>
<c:if test="${param.lang!=null && param.lang!=applicationScope.language}">
    <c:set var="language" value="${param.lang}" scope="application"/>
    <c:redirect context="${pageContext.request.contextPath}" url="/"/>
</c:if>
<%@include file="internatiolization.jsp" %>
<%@page import="by.minsk.training.ApplicationConstants" %>
<%@page import="by.minsk.training.ApplicationContext" %>
<c:set var="applicationContext" scope="session" value="${ApplicationContext.getInstance()}"/>
<c:set var="demonstrator" scope="page" value="${applicationContext.getBean('TableDemonstrator')}"/>
<h1><fmt:message key="startTitle" bundle="${bndle}"/></h1><br>
<div id="upper_right_corner">
    <c:if test="${sessionScope.user==null}">
        <a href="${pageContext.request.contextPath}/auth"><fmt:message key="login" bundle="${bndle}"/></a>
        <a href="${pageContext.request.contextPath}/reg"><fmt:message key="registration"
                                                                      bundle="${bndle}"/></a>
        <fmt:message key="langChange" bundle="${bndle}" var="chL" scope="application"/>
    </c:if>
    <c:if test="${sessionScope.user!=null}">
        <fmt:message key="LogStatus" bundle="${bndle}"/><c:out value="${sessionScope.user.name}"/>
        <form id="logout" action="${pageContext.request.contextPath}/" method="post">
            <input type="hidden" name="commandName" value="logoutUser">
            <input type="submit" value="logout"/>
        </form>
    </c:if>
    <form action="${pageContext.request.contextPath}/" method="get">
        <select name="lang">
            <option hidden disabled selected value><fmt:message key="languageSelection"
                                                                bundle="${bndle}"/></option>
            <option value="English">EN</option>
            <option value="Dutch">NL</option>
            <option value="German">DE</option>
            <option value="Russian">RU</option>
        </select>
        <input type="submit" value="${chL}"/>
    </form>
</div>
<c:if test="${sessionScope.user!=null}">
<a href="${pageContext.request.contextPath}/shipments"><fmt:message key="shipmentsLink" bundle="${bndle}"/></a>
<a href="${pageContext.request.contextPath}/tenders"><fmt:message key="tendersLink" bundle="${bndle}"/></a>
    <c:if test="${sessionScope.user.role.toString().equals('transport service provider')}">
        <a href="${pageContext.request.contextPath}/spot"><fmt:message key="spotLink" bundle="${bndle}"/></a>
    </c:if>
<a href="${pageContext.request.contextPath}/report"><fmt:message key="reportLink" bundle="${bndle}"/></a>
</c:if>
<br>
<h5>
Transport orders in focus
</h5>
<table>
<tr>
<th>Id</th>
<th>Tender</th>
<c:if test="${sessionScope.user.role.toString().equals('transport service provider')}"><th>Customer</th></c:if>
<th>State</th>
<th>Issued</th>
<th>Loading date</th>
<th>Source city</th>
<th>Source country</th>
<th>Destination city</th>
<th>Destination country</th>
<th>Goods</th>
<th>Weight</th>
</tr>
<c:choose>
<c:when test="${requestScope.page==null}">
<c:set var="items" scope="page" value="${demonstrator.divideCollection(sessionScope.ordersInFocus, 10)}"/>
<c:set var="page" scope="request" value="1"/>
</c:when>
</c:choose>
<c:forEach var="order" items="${items[page-1]}">
<tr>
<td><c:out value="${order.id}"/></td>
<td>
<c:choose>
<c:when test="${order.tender!=null}"><c:out value="${order.tender.title}"/></c:when>
<c:otherwise><c:out value="spot"/></c:otherwise>
</c:choose>
</td>
<c:if test="${sessionScope.user.role.toString().equals('transport service provider')}">
<td><c:choose>
<c:when test="${order.tender!=null}"><c:out value="${order.tender.customer.name}"/></c:when>
<c:otherwise><c:out value="${order.customer.name}"/></c:otherwise>
</c:choose></td>
</c:if>
<td><c:out value="${order.state}"/></td>
<td><c:out value="${order.issued}"/></td>
<td><c:out value="${order.loadingDate}"/></td>
<td><c:out value="${order.line.route.start.locality.name}"/></td>
<td><c:out value="${order.line.route.start.locality.country.name}"/></td>
<td><c:out value="${order.line.route.destination.locality.name}"/></td>
<td><c:out value="${order.line.route.destination.locality.country.name}"/></td>
<td><c:out value="${order.line.truckLoad.name}"/></td>
<td><c:out value="${order.line.truckLoad.weight}"/></td>
</tr>
</c:forEach>
</table>
<c:forEach var="page" items="${items.size}"><a href="${pageContext.request.contextPath}/?page=${request.page}&notificationsPage=${request.notificationsPage}"><c:out value=" ${ page}"/></a></c:forEach>
<br>
<%-- <%@page import="by.minsk.training.service.notification.NotificationScanner" %> --%>
<c:choose>
<c:when test="${request.notificationsPage==null}">
<c:set var=notifications scope="page" value="${demonstrator.divideCollection(sessionScope.notifications,10)}"/>
<c:set var=notificationsPage scope="page" value="1"/>
</c:when>
<c:otherwise></c:otherwise>
</c:choose>
<table>
<tr>
<th>Notification</th>
<th>Description</th>
<th>Reference</th>
</tr>
<c:forEach var="n" items="${page.notifications[notificationsPage-1]}">
 <tr>
 <td><c:out value="${n.type}"/></td>
 <td><c:out value="${n.description}"/></td>
 <td>
<c:choose>
<c:when test="${n.type.targetObject=='transportOrder'}">
<a href="${pageContext.request.contextPath}/shipments/shipment?shipmentid=${n.referencedObject}"><c:out value="shipment ${n.referencedObject}"/></a>
</c:when>
<c:when test="${n.type.targetObject=='tenderContest'}">
<a href="${pageContext.request.contextPath}/tenders/tender/contest?tenderid=${n.referencedObject}"><c:out value="tender ${n.referencedObject} contest"/></a>
</c:when>
<c:when test="${n.type.targetObject=='spotContest'}">
<a href="${pageContext.request.contextPath}/spot?shipmentid=${n.referencedObject}"><c:out value="shipment ${n.referencedObject} spot contest"/></a>
</c:when>
</c:choose>
</td>
 </tr>
</c:forEach>
</table>
<c:forEach var="notificationsPage" items="${items.size}"><a href="${pageContext.request.contextPath}/?page=${request.page}&notificationsPage=${request.notificationsPage}"><c:out value=" ${ page}"/></a></c:forEach>
</body>

