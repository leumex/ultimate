<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Tender</title>
    <meta charset="utf-8">
</head>
<body>
<%@include file="internatiolization.jsp" %>
<h1><fmt:message key="tenderDetailsTitle" bundle="${bndle}"/> <c:out value=" ${param.tenderId}"/></h1>
<%--it is still to be determined what way tender id value will be passed to the page from outside--%>
<h5>

    tender layout :
    tender terms, tender state, lines list,
    for finished tenders - actual TOs number per line,
    line nominees (links to company rendering form), overall tender KPI - to CUSTOMER users,
    customer (link to company rendering form) - to TSP users

</h5>
<h5>

    tender release form - to CUSTOMER users

</h5>
<h5>

    user tender participation result - to TSP users
    (how much TOs are received, how much TOs are accepted, user queries of tender lines, and which of them are awarded)
    (available since tender gets state 'contracted' and further)

</h5>
<a href="${pageContext.request.contextPath}/tenders/contest/tender"><fmt:message key="tenderContestLink" bundle="${bndle}"/></a>
    <%--available once tender gets state 'released'--%>
<br>
<a href="${pageContext.request.contextPath}/shipments/create"><fmt:message key="shipmentDesignLink" bundle="${bndle}"/></a>
<%--available once tender gets state 'contracted' and till it gets state 'completed'
new shipment will be within the scope of the tender--%>
<br>
<h5>

    user's ongoing TOs ('assigned', 'accepted', 'in_action') that refers to the tender,
    including TO opening function (link to shipment rendering page) - to TSP users

</h5>
<a href="${pageContext.request.contextPath}/tenders"><fmt:message key="tendersLink" bundle="${bndle}"/></a>
<br>
<a href="${pageContext.request.contextPath}/"><fmt:message key="startPageLink" bundle="${bndle}"/></a>
</body>
</html>
