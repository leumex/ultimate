<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Tenders</title>
</head>
<body>
<%@include file="internatiolization.jsp" %>
<h1><fmt:message key="tendersLink" bundle="${bndle}"/></h1>
<br>
<h5>
    table with (

    all tenders ever set by user - to CUSTOMER user,

    all tenders ever participated by user - to TSP user

    ) , each tender has link to tender rendering page
</h5>
<br>
<c:if test="${sessionScope.user.role.toString() == 'customer'}">
    <a href="${pageContext.request.contextPath}/tenders/create"><fmt:message key="tenderDesignLink"
                                                                             bundle="${bndle}"/></a>
</c:if>
<br>
<a href="${pageContext.request.contextPath}/"><fmt:message key="startPageLink" bundle="${bndle}"/></a>
</body>
</html>
