<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page isErrorPage="true"%>
<html>
<head>
    <title>Oops...</title>
</head>
<body>
<h3>${pageContext.errorData.requestURI} has not been executed successfully</h3>
Error code: ${pageContext.errorData.statusCode}
Error message: ${pageContext.exception.message}

<a href="${pageContext.request.contextPath}/">back to main page</a>
</body>
</html>
