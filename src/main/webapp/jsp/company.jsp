<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<html>
<head>
<title>Company</title>
</head>
<body>
	<%@include file="internatiolization.jsp"%>
	<h1>
		<fmt:message key="company" bundle="${bndle}" />
	</h1>
	<%@page import="by.minsk.training.ApplicationConstants"%>
	<%@page import="by.minsk.training.ApplicationContext"%>
	<c:set var="userService" scope="page"
		value="${AplicationContext.getBean('UserServiceImpl'}" />
	<c:set var="userService" scope="page"
		value="${AplicationContext.getBean('ContractServiceImpl'}" />
	<c:set var="company" scope="page"
		value="${userService.getById(request.company_id}" />
	<c:set var="user" scope="page" value="${sessionScope.user}" />
	Ongoing contract is:
	<c:choose>
		<c:when test="${user.role.toString() == 'customer'}">
			<c:out value="${contractService.defineContract(user, company)}" />
		</c:when>
		<c:when test="${user.role.toString() == 'transport service provider'}">
			<c:out value="${contractService.defineContract(company, user)}" />
		</c:when>
		<c:otherwise>not found</c:otherwise>
	</c:choose>

	<h5>
		includes
		<!-- detailed layout of chosen contractor (contract
		information),  -->
		including for TSP users: number of tenders CUSTOMER has set by the
		time of view, total number of completed actual TOs year by year for
		CUSTOMER users: number of tenders TSP participated totally, number of
		TOs TSP received year by year, number of TOs TSP accepted year by
		year, number of TOs TSP completed year by year
	</h5>

	<a href="${pageContext.request.contextPath}/report"><fmt:message
			key="reportLink" bundle="${bndle}" /></a>
	<a href="${pageContext.request.contextPath}/"><fmt:message
			key="startPageLink" bundle="${bndle}" /></a>
</body>
</html>
