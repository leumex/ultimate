<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
</head>
<body>
<br>
<%@include file="internatiolization.jsp"%>
<h1><fmt:message key="auth" bundle="${bndle}"/></h1>

<fmt:message key="userLoginPlaceHolder" bundle="${bndle}" var="ul"/>
<fmt:message key="userPasswordPlaceHolder" bundle="${bndle}" var="up"/>

<form action="${pageContext.request.contextPath}/" method="post">
    <input type="hidden" name="commandName" value="loginUser"/>
   <label for="lgn">Your login, please :</label> <input id="lgn" type="text" placeholder="${ul}" name="Login"/>
    <label for="pswrd">Your password, please :</label><input id="pswrd" type="text" placeholder="${up}" name="Password"/>
    <button type="submit"><fmt:message key="login" bundle="${bndle}"/></button>
</form>

<br>
<a href="${pageContext.request.contextPath}/reg"><fmt:message key="registration" bundle="${bndle}"/></a>

<br><c:if test="${requestScope.loginSuccess}">
    <fmt:message key="LogStatus" bundle="${bndle}" var="logged"/>
    <c:out value="${logged} ${sessionScope.user.name}"/>
</c:if>
<c:if test="${requestScope.loginFailure}">
    <fmt:message key="loginFailure" bundle="${bndle}"/>
</c:if>
<br><c:if test="${sessionScope.warning!=null}">
    <c:out value="${sessionScope.warning}"/>
</c:if>
<br><a href="${pageContext.request.contextPath}/"><fmt:message key="startPageLink" bundle="${bndle}"/></a>
</body>
