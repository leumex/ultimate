<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>New shipment</title>
</head>
<body>
<%@include file="internatiolization.jsp" %>
<h1><fmt:message key="shipmentDesignTitle" bundle="${bndle}"/></h1>
<h5>

    optional tender selection - to CUSTOMER users

</h5>
<h5>

    optional line selection - to CUSTOMER users

</h5>
<h5>

    line design form

</h5>
<h5>

    shipment design form

</h5>
once new shipment is submitted,
<form action="${pageContext.request.contextPath}/shipments/shipment" method="get">
    <input type="hidden" name="shipmentId" value="${pageScope.shipment.id}"/>
    <fmt:message var="view" key="viewNewShipment" bundle="${bndle}"/>
    <input type="submit" value="${pageScope.view}">
    <%--the way new shipment' id value is passed to shipment tendering page may be modified--%>
</form>
<a href="${pageContext.request.contextPath}/shipments"><fmt:message key="shipmentsLink" bundle="${bndle}"/></a>
</body>
</html>
