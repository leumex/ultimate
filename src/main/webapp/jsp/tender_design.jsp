<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>New tender</title>
</head>
<body>
<%@include file="internatiolization.jsp" %>
<h1><fmt:message key="newTender" bundle="${bndle}"/></h1>
<h5>

    Tender design form

</h5>
<h5>

    Lines selection form

</h5>
<h5>

    Line design form

</h5>
<h6>
    Country adding button
</h6>
<h6>
    Locality adding button
</h6>
<h6>
    Address adding button
</h6>
<form action="${pageContext.request.contextPath}/tenders/tender" method="get">
    <input type="hidden" name="tenderId" value="${pageScope.tender.id}"/>
    <fmt:message var="openTender" scope="page" key="openNewTender" bundle="${bndle}"/>
    <label for="newTenderLink">once new tender is submitted,</label><input id="newTenderLink" type="submit" value="${pageScope.openTender}"/>
</form>
<%--it is still not a final decision, what way the id of new tender will be passed to a tender rendering page--%>
<a href="${pageContext.request.contextPath}/tenders"><fmt:message key="backToTenders" bundle="${bndle}"/></a>
</body>
</html>
