function createLine(){
    document.getElementById("commandName").value=document.getElementById("createLine").value;
    this.form.submit();
}

function createTender(){
    document.getElementById("tender").submit();
}

function deleteLine(){
    document.getElementById("commandName").value = document.getElementById("removeLine").value;
    document.getElementById("uniform").submit();
}

function renderCountryLocalities() {
    document.getElementById("_country").value = document.getElementById("addingAddressCountry").value;
    document.getElementById("uniform").action="";
    document.getElementById("uniform").submit();
}

function tenderDetailsSubmit() {
    document.getElementById("uniform").action="";
    document.getElementById("tName").value = document.getElementById("tenderTitle").value;
    document.getElementById("tStartDate").value = document.getElementById("tenderStartDate").value;
    document.getElementById("tEndDate").value = document.getElementById("tenderEndDate").value;
    document.getElementById("tTrips").value = document.getElementById("tenderTripsQuantity").value;
    document.getElementById("uniform").submit();
}

function changeLang() {
    document.getElementById('logout').submit();
    return false;
}

function retrieveNewCountryInput() {
    document.getElementById("commandName").value = document.getElementById("addingCountryCommand").value;
    document.getElementById("_country").value = document.getElementById("addingCountry").value;
    document.getElementById("uniform").submit();
}

function retrieveNewLocalityInput() {
    document.getElementById("commandName").value = document.getElementById("addingLocalityCommand").value;
    document.getElementById("_country").value = document.getElementById("addingLocalityCountry").value;
    document.getElementById("_locality").value = document.getElementById("addingLocality").value;
    document.getElementById("uniform").submit();
}

function retrieveNewAddressInput() {
    document.getElementById("commandName").value = document.getElementById("addingAddressCommand").value;
    document.getElementById("_country").value = document.getElementById("addingAddressCountry").value;
    document.getElementById("_locality").value = document.getElementById("addingAddressLocality").value;
    document.getElementById("address").value = document.getElementById("_address").value;
    document.getElementById("uniform").submit();
}