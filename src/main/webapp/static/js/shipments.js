function sortById(){
	document.getElemenyById("comparingParameter").value = 'id';
	document.getElementById("command").value = 'sortOrders';
	document.getElemenyById("transportOrder").submit();
}
function sortByDate(){
	document.getElemenyById("comparingParameter").value = 'date';
	document.getElementById("command").value = 'sortOrders';
	document.getElemenyById("transportOrder").submit();
}
function sortByState(){
	document.getElemenyById("comparingParameter").value = 'state';
	document.getElementById("command").value = 'sortOrders';
	document.getElemenyById("transportOrder").submit();
}
function sortByResponse(){
	document.getElemenyById("comparingParameter").value = 'response';
	document.getElementById("command").value = 'sortOrders';
	document.getElemenyById("transportOrder").submit();
}
function sortByTender(){
	document.getElemenyById("comparingParameter").value = 'tender';
	document.getElementById("command").value = 'sortOrders';
	document.getElemenyById("transportOrder").submit();
}
function sortByCustomer(){
	document.getElemenyById("comparingParameter").value = 'customer';
document.getElementById("command").value = 'sortOrders';
document.getElemenyById("transportOrder").submit();
}
function sortByPickupDate(){
	document.getElemenyById("comparingParameter").value = 'pick_up_date';
	document.getElementById("command").value = 'sortOrders';
	document.getElemenyById("transportOrder").submit();
}
function sortByRoute(){
	document.getElemenyById("comparingParameter").value = 'route';
	document.getElementById("command").value = 'sortOrders';
	document.getElemenyById("transportOrder").submit();
}

function filterById(){
	document.getElementById("command").value='orderFilterCommand';
	document.getElementById("filterParameter").value='id';
	document.getElementById("transportOrder").submit();
}
function filterByDate(){
	document.getElementById("command").value='orderFilterCommand';
	document.getElementById("filterParameter").value='date';
	document.getElementById("transportOrder").submit();
}
function filterByState(){
	document.getElementById("command").value='orderFilterCommand';
	document.getElementById("filterParameter").value='state';
	document.getElementById("transportOrder").submit();
}

function filterByResponse(){  //is used by tsp to handle 3rd column of transport orders table, and is used by customers and admins to handle 4th column
	document.getElementById("command").value='orderFilterCommand';
	document.getElementById("filterParameter").value='response';
	document.getElementById("transportOrder").submit();
}

function filterByTender(){
	document.getElementById("command").value='orderFilterCommand';
	document.getElementById("filterParameter").value='tender';
	document.getElementById("transportOrder").submit();
}

function filterByCustomer(){
	document.getElementById("command").value='orderFilterCommand';
	document.getElementById("filterParameter").value='customer';
	document.getElementById("transportOrder").submit();
}

function filterByPickupDate(){
	document.getElementById("command").value='orderFilterCommand';
	document.getElementById("filterParameter").value='pickupDate';
	document.getElementById("transportOrder").submit();
}

function filterByRoute(){
	document.getElementById("command").value='orderFilterCommand';
	document.getElementById("filterParameter").value='route';
	document.getElementById("transportOrder").submit();
}

function acceptTransportOrder(){
	document.getElementById("command").value='orderAcceptCommand';
	document.getElementById("transportOrder").submit();
}

function rejectTransportOrder(){
	document.getElementById("command").value='orderRejectCommand';
	document.getElementById("transportOrder").submit();
}

function recallTransportOrder(){
	document.getElementById("command").value='orderRecallCommand';
	document.getElementById("transportOrder").submit();
}

function deleteTransortOrder(){
	document.getElementById("command").value='orderDeleteCommand';
	document.getElementById("transportOrder").submit();
}









