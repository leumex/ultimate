package by.minsk.training.dao;

import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.core.Bean;

@Bean
public class TransactionManagerImpl implements TransactionManager {

    private DataSource dataSource;
    private ThreadLocal<Connection> localConnection = new ThreadLocal<>();
    private static final Logger logger = LogManager.getLogger(TransactionManagerImpl.class);

    public TransactionManagerImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void beginTransaction() throws SQLException {
        if (localConnection.get() == null) {
            Connection connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            localConnection.set(connection);
        } else {
            logger.warn("Transaction has already started");
        }
    }

    @Override
    public void commitTransaction() throws SQLException {
        Connection connection = localConnection.get();
        if (connection != null) {
            connection.commit();
            connection.close();
        }
        localConnection.remove();
    }

    @Override
    public void rollbackTransaction() throws SQLException {
        Connection connection = localConnection.get();
        if (connection != null) {
            connection.rollback();
            connection.close();
        }
        localConnection.remove();
    }

    public Connection getConnection() throws SQLException {
        if (localConnection.get() != null) {
            return (Connection) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[]{Connection.class},
                    (proxy, method, args) -> {
                        if (method.getName().equals("close")) {
                            return null;
                        } else {
                            Connection realConnection = localConnection.get();
                            return method.invoke(realConnection, args);
                        }
                    });
        } else {
            return dataSource.getConnection();
        }
    }
}
