package by.minsk.training.dao.basic;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.ConnectionManager;
import by.minsk.training.dao.TransactionSupport;
import by.minsk.training.dao.Transactional;
import by.minsk.training.entity.Address;
import by.minsk.training.entity.Route;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


@Bean
@AllArgsConstructor
@TransactionSupport
public class RouteDAOImpl implements RouteDAO {

    private static final Logger logger = LogManager.getLogger(RouteDAOImpl.class);
    private final ConnectionManager connectionManager;

    private static final String INSERT_QUERY = "insert into route (start, destination, transit_conditions) values (?,?,?);";
    private static final String UPDATE_QUERY = "update route set start=?, destination=?, transit_conditions=? where id =?;";
    private static final String SELECT_BY_ID_QUERY = "select route.transit_conditions, route.start," +
            "       route.destination from route where route.id = ?;";
    private static final String CHECK_ADDRESS_RECORD_QUERY = "select exists(select*from address where address.id = ?);";
    private static final String SELECT_ALL_RECORDS_QUERY = "select * from route;";
    private static final String SELECT_BY_ALL_PARAMS = "select id from route where start = ? and destination = ? and transit_conditions = ?;";
	@Override
	public Long save(Route entity) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public boolean update(Route entity) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean delete(Route entity) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public Route getById(Long id) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<Route> findAll() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}
}
