package by.minsk.training.dao.basic;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.ConnectionManager;
import by.minsk.training.dao.TransactionSupport;
import by.minsk.training.dao.Transactional;
import by.minsk.training.entity.Tender;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@TransactionSupport
@Bean
public class TenderDAOImpl implements TenderDAO {

    private final ConnectionManager connectionManager;
    private static final Logger logger = LogManager.getLogger(TenderDAOImpl.class);
    private static final String INSERT_QUERY = "insert into tender (title, customer_id, set_date, release_date, competition_deadline, " +
            "tender_start, tender_end, estimated_quantity, state) values (?,?,?, ?, ?, ?, ?,?,?);";
    private static final String UPDATE_QUERY = "update tender set title = ?, release_date = ?, competition_deadline = ?," +
            "                  tender_start = ?, tender_end = ?," +
            "                  estimated_quantity = ?, state = ?," +
            "                  customer_id = ?" +
            "where id = ?;";
    private static final String SELECT_BY_ID_STATEMENT = "select title, customer_id, set_date, release_date, competition_deadline," +
            "tender_start, tender_end, estimated_quantity, state" +
            " from tender" +
            " where id = ?;";
    private static final String SELECT_ALL_STATEMENT = "select * from tender;";
	/*
	 * private static final String GET_TENDER_LINES =
	 * "select line_id from tender_has_lines where tender_id = ? ;";
	 */
    private static final String CHECK_RECORDS_STATEMENT = "select exists (select*from user where id = ?);";
    private static final String ASSIGN_LINE_STATEMENT = "insert into tender_has_line (tender_id, line_id) values (?,?);";
    private static final String CHECK_LINE_PRESENCE = "select count(1) from line where id = (?);";
    private static final String SELECT_BY_CUSTOMER_STATEMENT = "select * from tender where customer_id =?;";
	@Override
	public Long save(Tender entity) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public boolean update(Tender entity) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean delete(Tender entity) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public Tender getById(Long id) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<Tender> findAll() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}
}
