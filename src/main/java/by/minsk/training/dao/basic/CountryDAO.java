package by.minsk.training.dao.basic;

import by.minsk.training.dao.CRUDDao;
import by.minsk.training.entity.Country;

import java.sql.SQLException;

public interface CountryDAO extends CRUDDao<Country, Long> {
    
}
