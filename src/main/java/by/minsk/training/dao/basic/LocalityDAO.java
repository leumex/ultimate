package by.minsk.training.dao.basic;

import by.minsk.training.dao.CRUDDao;
import by.minsk.training.entity.Country;
import by.minsk.training.entity.Locality;

import java.sql.SQLException;
import java.util.List;

public interface LocalityDAO extends CRUDDao<Locality,Long> {
    
}
