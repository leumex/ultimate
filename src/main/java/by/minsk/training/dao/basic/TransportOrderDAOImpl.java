package by.minsk.training.dao.basic;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.ConnectionManager;
import by.minsk.training.dao.Transactional;
import by.minsk.training.entity.TransportOrder;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Bean
public class TransportOrderDAOImpl implements TransportOrderDAO {

	private final ConnectionManager connectionManager;

	private static final Logger logger = LogManager.getLogger(TransportOrderDAOImpl.class);

	private static final String INSERT_QUERY = "insert into transport_order (issued, loading_date, "
			+ "desired_delivery_term, line, tender_id) values (?, ?, ?, ?, ?);";
	private static final String UPDATE_QUERY = "update transport_order "
			+ "set issued=?, loading_date = ?, desired_delivery_term = ?, state = ?, "
			+ "    line = ?, tender_id = ?, customer_id = ?" + "where id = ?;";
	private static final String SELECT_BY_ID_STATEMENT = "select issued, loading_date, desired_delivery_term,"
			+ "line, tender_id, customer_id, state " + "from transport_order " + "where id = ?;";
	private static final String SELECT_ALL_STATEMENT = "select * " + "from transport_order;";
	private static final String CHECK_LINE_RECORD_STATEMENT = "select exists(select * from line where id = ?);";
	private static final String CHECK_TENDER_RECORD_STATEMENT = "select exists(select * from tender where id = ?);";
	private static final String SELECT_UNSETTLED_SHIPMENTS = "select * from transport_order "
			+ "where state is 'ASSIGNED'";
	@Override
	public Long save(TransportOrder entity) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public boolean update(TransportOrder entity) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean delete(TransportOrder entity) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public TransportOrder getById(Long id) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<TransportOrder> findAll() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}
}
