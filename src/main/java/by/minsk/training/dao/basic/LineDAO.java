package by.minsk.training.dao.basic;

import by.minsk.training.dao.CRUDDao;
import by.minsk.training.entity.Line;

import java.sql.SQLException;
import java.util.Set;

public interface LineDAO extends CRUDDao<Line, Long> {

}
