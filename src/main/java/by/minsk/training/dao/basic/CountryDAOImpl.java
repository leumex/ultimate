package by.minsk.training.dao.basic;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.ConnectionManager;
import by.minsk.training.entity.Country;
import lombok.AllArgsConstructor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Bean
@AllArgsConstructor
public class CountryDAOImpl implements CountryDAO {

	private static final String INSERT_QUERY = "insert into country (name) value (?);";
	private static final String SELECT_BY_ID_QUERY = "select name from  country where id = ? ;";
	private static final String SELECT_BY_NAME_QUERY = "select id from  country where name = ? ;";
	private static final String GET_ALL_QUERY = "select id, name from country;";

	private ConnectionManager connectionManager;
	private static final Logger logger = LogManager.getLogger(CountryDAOImpl.class);

	@Override
	public Long save(Country entity) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean update(Country entity) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(Country entity) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Country getById(Long id) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Country> findAll() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

}
