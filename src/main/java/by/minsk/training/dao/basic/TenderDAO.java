package by.minsk.training.dao.basic;

import by.minsk.training.dao.CRUDDao;
import by.minsk.training.entity.Tender;

import java.sql.SQLException;
import java.util.List;

public interface TenderDAO extends CRUDDao<Tender, Long> {

}
