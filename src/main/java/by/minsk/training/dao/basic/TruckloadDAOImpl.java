package by.minsk.training.dao.basic;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.ConnectionManager;
import by.minsk.training.entity.Truckload;
import lombok.AllArgsConstructor;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Bean
public class TruckloadDAOImpl implements TruckloadDAO {

	private ConnectionManager connectionManager;

	private static final String INSERT_QUERY = "insert into truckload (name, weight, load_units, truck_type) values (?,?,?,?);";
	private static final String UPDATE_QUERY = "update truckload set name=?, weight=?, load_units=?, truck_type=? where id =?;";
	private static final String SELECT_BY_ID_QUERY = "select name, weight, load_units, truck_type from truckload where id = ?;";
	private static final String SELECT_ALL_QUERY = "select id, name, weight,load_units, truck_type from truckload;";

	@Override
	public Long save(Truckload entity) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean update(Truckload entity) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(Truckload entity) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Truckload getById(Long id) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Truckload> findAll() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

}
