package by.minsk.training.dao.basic;

import by.minsk.training.dao.CRUDDao;
import by.minsk.training.entity.Address;
import by.minsk.training.entity.Route;

import java.sql.SQLException;

public interface RouteDAO extends CRUDDao<Route, Long> {
   
}
