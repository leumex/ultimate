package by.minsk.training.dao.basic;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.ConnectionManager;
import by.minsk.training.dao.TransactionSupport;
import by.minsk.training.dao.Transactional;
import by.minsk.training.entity.Address;
import lombok.AllArgsConstructor;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Bean
@TransactionSupport
public class AddressDAOImpl implements AddressDAO {

	private final ConnectionManager connectionManager;

	@Override
	public Long save(Address entity) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean update(Address entity) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(Address entity) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Address getById(Long id) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Address> findAll() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}
}
