package by.minsk.training.dao.basic;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.ConnectionManager;
import by.minsk.training.dao.Transactional;
import by.minsk.training.entity.*;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.*;

@AllArgsConstructor
@Bean
public class LineDAOImpl implements LineDAO {

    private static final Logger logger = LogManager.getLogger(LineDAOImpl.class);
    private final ConnectionManager connectionManager;

    private static final String INSERT_QUERY = "insert into line (truckload, route_line) values (?,?);";
    private static final String UPDATE_QUERY = "update line set truckload=?, truckload=? where id=?;";
    private static final String SELECT_BY_ID_QUERY = "select id, truckload, route_line from line" +
            " where id = ?;";
    private static final String GET_ALL_QUERY = "select * from line;";
    private static final String SELECT_TENDER_LINES = "select * from line \n" +
            "inner join  tender_has_line on line.id = tender_has_line.line_id\n" +
            "where tender_has_line.tender_id = ? ;";
    private static final String SELECT_START_LOCATION = "select lc.name from line l " +
            "inner join route r on r.id = l.route_line " +
            "inner join address a on r.start = a.id " +
            "inner join locality lc on a.locality_id = lc.id " +
            "where l.id = ?;";
    private static final String SELECT_END_LOCATION = "select lc.name from line l " +
             "inner join route r on r.id = l.route_line " +
             "inner join address a on r.destination = a.id " +
             "inner join locality lc on a.locality_id = lc.id " +
             "where l.id = ?;";
	@Override
	public Long save(Line entity) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public boolean update(Line entity) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean delete(Line entity) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public Line getById(Long id) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<Line> findAll() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}
}
