package by.minsk.training.dao.basic;

import by.minsk.training.dao.CRUDDao;
import by.minsk.training.entity.Address;

import java.sql.SQLException;
import java.util.List;

public interface AddressDAO extends CRUDDao<Address, Long> {

}
