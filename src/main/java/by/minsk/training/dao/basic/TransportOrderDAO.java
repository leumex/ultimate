package by.minsk.training.dao.basic;

import by.minsk.training.dao.CRUDDao;
import by.minsk.training.entity.TransportOrder;

import java.sql.SQLException;
import java.util.List;

public interface TransportOrderDAO extends CRUDDao<TransportOrder,Long> {
}
