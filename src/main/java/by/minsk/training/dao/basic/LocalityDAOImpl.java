package by.minsk.training.dao.basic;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.ConnectionManager;
import by.minsk.training.dao.TransactionSupport;
import by.minsk.training.dao.Transactional;
import by.minsk.training.entity.Country;
import by.minsk.training.entity.Locality;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@TransactionSupport
@AllArgsConstructor
@Bean
public class LocalityDAOImpl implements LocalityDAO {

	private final ConnectionManager connectionManager;
	private static final Logger logger = LogManager.getLogger(LocalityDAOImpl.class);

	private static final String SAVE_QUERY = "insert into locality(name, country_id) values(?,?);";
	private static final String UPDATE_QUERY = "update locality set name=?, country_id=? where id =?;";
	private static final String SELECT_BY_ID_QUERY = "select name, country_id from locality where id = ?;";
	private static final String GET_ALL_QUERY = "select * from locality";
	private static final String GET_BY_COUNTRY_NAME_QUERY = "select locality.id, locality.name, locality.country_id "
			+ "from locality inner join country on locality.country_id = country.id " + "where country.name = ?;";
	private static final String CHECK_RECORD_QUERY = "select exists(select * from country where id = ?);";
	private static final String GET_LOCALITIES_OF_CERTAIN_COUNTRY_QUERY = "select id from locality where country_id = ?;";

	@Override
	public Long save(Locality entity) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean update(Locality entity) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(Locality entity) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Locality getById(Long id) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Locality> findAll() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

}
