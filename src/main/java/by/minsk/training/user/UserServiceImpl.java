package by.minsk.training.user;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.Transactional;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mindrot.jbcrypt.BCrypt;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Bean
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private static final Logger logger = LogManager.getLogger(UserServiceImpl.class);

    private UserDao userDao;

    @Override
    public UserEntity selectCompany(Long id) {
    	try {
    		return userDao.getById(id);
    	}catch (SQLException e) {
    		logger.error ("Failed to retrieve UserEntity record by Lond id parameter");
    		return null;
    	}
    }

    @Override
    public boolean loginUser(UserEntity entity) {
        Optional<UserEntity> byLogin;
        try {
            byLogin = userDao.findByLogin(entity.getLogin());
            logger.debug("UserDao#findBylogin has completed execution");
        } catch (SQLException e) {
            logger.error("Failed to read user", e);
            byLogin = Optional.empty();
        }
        return byLogin.filter(user -> user.getPassword().equals(BCrypt.hashpw(entity.getPassword(),user.getSalt()))).isPresent();
    }


    @Override
    @Transactional
    public boolean registerUser(UserEntity entity) {
        try {
            userDao.save(entity);
            return true;
        } catch (SQLException e) {
            logger.error("Failed to save user", e);
            return false;
        }
    }

    @Override
    public List<UserEntity> getAllUsers() {
        try {
            return userDao.findAll();
        } catch (SQLException e) {
            logger.error("Failed to read users", e);
            return new ArrayList<>();
        }
    }

    @Override
    public UserEntity findUser(String login) {
        try {
            Optional<UserEntity> result = userDao.findByLogin(login);
             return (result.orElse(null));
        } catch (SQLException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }
}
