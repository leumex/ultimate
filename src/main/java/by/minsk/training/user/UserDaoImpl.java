package by.minsk.training.user;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.minsk.training.core.Bean;
import by.minsk.training.dao.ConnectionManager;
import lombok.AllArgsConstructor;

@Bean
@AllArgsConstructor
public class UserDaoImpl implements UserDao {

    private final static Logger logger = LogManager.getLogger(UserDaoImpl.class);
	/* private final static AtomicLong COUNTER = new AtomicLong(1); */

    private static final String SELECT_ALL_QUERY = "select * from user;";
    private static final String INSERT_QUERY = "insert into user(name, role, login, password, salt, registration_date) values (?,?,?,?,?,?);";
    private static final String UPDATE_QUERY = "update user set name=?, login = ?, password = ?, salt = ? where id = ?;";
    private static final String DELETE_QUERY = "delete from user where id=?;";
    private static final String SELECT_BY_ID_QUERY = "select name, login, password, salt, role, registration_date from  user where id = ? ;";
    private static final String SELECT_BY_LOGIN_QUERY = "select * from user where login = ?;";


    private ConnectionManager connectionManager;

    @Override
    public Optional<UserEntity> findByLogin(String login) throws SQLException {
        List<UserEntity> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_BY_LOGIN_QUERY)) {
            logger.debug("UserDao#findBylogin is invoked..");
            selectStmt.setString(1, login);
            ResultSet resultSet = selectStmt.executeQuery();
            while (resultSet.next()) {
                UserEntity entity = parseResultSet(resultSet);
                result.add(entity);
            }
        }
        return result.stream().findFirst();
    }

    @Override
    public Long save(UserEntity entity) throws SQLException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement insertStatement = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS)) {
            int i = 0;
            insertStatement.setString(++i, entity.getName());
            insertStatement.setString(++i, entity.getRole().toString());
            insertStatement.setString(++i, entity.getLogin());
            insertStatement.setString(++i, entity.getPassword());
            insertStatement.setString(++i, entity.getSalt());
            insertStatement.setDate(++i, Date.valueOf(entity.getRegistrationDate()));
            insertStatement.executeUpdate();
            ResultSet generatedKeys = insertStatement.getGeneratedKeys();
            while (generatedKeys.next()) {
                entity.setId(generatedKeys.getLong(1));
            }
        }
        return entity.getId();
    }

    @Override
    public boolean update(UserEntity entity) throws SQLException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement updateStmt = connection.prepareStatement(UPDATE_QUERY)) {
            int i = 0;
            updateStmt.setString(++i, entity.getName());
            updateStmt.setString(++i, entity.getLogin());
            updateStmt.setString(++i, entity.getPassword());
            updateStmt.setString(++i, entity.getSalt());
            updateStmt.setString(++i, entity.getRole().toString());
            updateStmt.setLong(++i, entity.getId());
            return updateStmt.executeUpdate() > 0;
        }
    }

    @Override
    public boolean delete(UserEntity entity) throws SQLException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement updateStmt = connection.prepareStatement(DELETE_QUERY)) {
            updateStmt.setLong(1, entity.getId());
            return updateStmt.executeUpdate() > 0;
        }
    }

    @Override
    public UserEntity getById(Long id) throws SQLException {
        List<UserEntity> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_BY_ID_QUERY)) {
            selectStmt.setLong(1, id);
            ResultSet resultSet = selectStmt.executeQuery();
            while (resultSet.next()) {
                UserEntity entity = parseResultSet(resultSet);
                result.add(entity);
            }
        }
        return result.stream().findFirst().orElseThrow(() -> new IllegalArgumentException("Entity not found with given id: " + id));
    }

    @Override
    public List<UserEntity> findAll() throws SQLException {
        List<UserEntity> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_ALL_QUERY)) {
            ResultSet resultSet = selectStmt.executeQuery();
            while (resultSet.next()) {
                UserEntity entity = parseResultSet(resultSet);
                result.add(entity);
            }
        }
        return result;
    }

    private UserEntity parseResultSet(ResultSet resultSet) throws SQLException {
        long entityId = resultSet.getLong("id");
        String login = resultSet.getString("login");
        String password = resultSet.getString("password");
        String salt = resultSet.getString("salt");
        String name = resultSet.getString("name");
        UserEntity.Roles role = UserEntity.Roles.getRole(resultSet.getString("role").toLowerCase());
        LocalDate registrationDate = resultSet.getDate("registration_date").toLocalDate();
        return new UserEntity.Builder()
                .setId(entityId)
                .setLogin(login)
                .setPassword(password)
                .setSalt(salt)
                .setName(name)
                .setRole(role)
                .setRegistrationDate(registrationDate)
                .build();
    }
}
