package by.minsk.training.user;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.NoSuchElementException;

@Data
/* @Builder */
@NoArgsConstructor
/* @AllArgsConstructor */
public class UserEntity {
	private Long id;
	private UserEntity.Roles role;
	private String login;
	private String name;
	private LocalDate registrationDate;
	private String password;
	private String salt;

	public enum Roles {
		CUSTOMER("customer"), TRANSPORT_SERVICE_PROVIDER("transport service provider"), ADMIN("admin"), ALL("visitor");

		String representation;

		Roles(String representation) {
			this.representation = representation;
		}

		static Roles getRole(String name) throws SQLException {
			Roles definedValue;
			try {
				definedValue = Arrays.stream(Roles.values()).filter(role -> role.toString().equals(name)).findFirst()
						.get();
			} catch (NoSuchElementException e) {
				throw new SQLException("Unknown role", e);
			}
			return definedValue;
		}

		@Override
		public String toString() {
			return this.representation;
		}
	}

	public static class Builder {
		private UserEntity entity = new UserEntity();

		public UserEntity build() {
			return entity;
		}

		public Builder setId(Long id) {
			entity.setId(id);
			return this;
		}

		public Builder setLogin(String login) {
			entity.setLogin(login);
			return this;
		}

		public Builder setPassword(String password) {
			entity.setLogin(password);
			return this;
		}

		public Builder setSalt(String salt) {
			entity.setLogin(salt);
			return this;
		}

		public Builder setName(String name) {
			entity.setLogin(name);
			return this;
		}

		public Builder setRole(Roles role) {
			entity.setRole(role);
			return this;
		}

		public Builder setRegistrationDate(LocalDate registrationDate) {
			entity.setRegistrationDate(registrationDate);
			return this;
		}
	}
}
