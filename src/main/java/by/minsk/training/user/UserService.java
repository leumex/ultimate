package by.minsk.training.user;

import java.util.List;

public interface UserService {

    boolean loginUser(UserEntity entity);

    boolean registerUser(UserEntity entity);

    UserEntity findUser(String login);

    List<UserEntity> getAllUsers();
    
    UserEntity selectCompany(Long id);
}
