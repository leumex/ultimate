package by.minsk.training.user;

import by.minsk.training.dao.CRUDDao;

import java.sql.SQLException;
import java.util.Optional;

public interface UserDao extends CRUDDao<UserEntity, Long> {

Optional<UserEntity> findByLogin (String login) throws SQLException;
}
