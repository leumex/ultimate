package by.minsk.training;

public class ApplicationConstants {
    public static final String REG_NEW_USER = "registerSaveUser";
    public static final String LOGIN_USER = "loginUser";
    public static final String LOGOUT_USER = "logoutUser";
    public static final String ORDER_SORT_COMMAND = "sortOrders";
    
}
