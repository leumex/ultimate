package by.minsk.training.command;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static by.minsk.training.ApplicationConstants.ORDER_SORT_COMMAND;

import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import by.minsk.training.core.Bean;
import by.minsk.training.entity.TransportOrder;
import by.minsk.training.entity.TspPerformance.TspResponse;
import by.minsk.training.service.ServiceException;
import by.minsk.training.service.TransportOrderService;
import lombok.AllArgsConstructor;

@Bean(name = ORDER_SORT_COMMAND)
@AllArgsConstructor
public class OrderSortCommand implements ServletCommand {

	private TransportOrderService transportOrderService;
	private static final Logger logger = LogManager.getLogger(OrderSortCommand.class);

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {

		// validation of incoming parameters

		Set<TransportOrder> orders = (Set<TransportOrder>) req.getSession().getAttribute("orders");
		String comparingParameter = req.getParameter("comparingParameter").trim();
		Comparator<TransportOrder> comparator;
		if (comparingParameter.equals("id")) {
			comparator = (o1, o2) -> (int) (o1.getId() - o2.getId());
		} else if (comparingParameter.equals("date")) {
			comparator = (o1, o2) -> o1.getIssued().compareTo(o2.getIssued());
		} else if (comparingParameter.equals("state")) {
			comparator = (o1, o2) -> o1.getState().compareTo(o2.getState());
		} else if (comparingParameter.equals("response")) {
			comparator = (o1, o2) -> {
				try {
					return transportOrderService.getLatestAssignment(o1).getTspResponse()
							.compareTo(transportOrderService.getLatestAssignment(o2).getTspResponse());
				} catch (ServiceException e) {
					throw new RuntimeException(
							"occured during orders sorting by TspResponse value of an order most recent assignment", e);
				}
			};
		} else if (comparingParameter.equals("tender")) {
			comparator = (o1, o2) -> o1.getTender().getTitle().compareToIgnoreCase(o2.getTender().getTitle());
		}

		else if (comparingParameter.equals("customer")) {
			comparator = (o1, o2) -> {
				String customer1 = o1.getTender() != null ? o1.getTender().getCustomer().getName()
						: o1.getCustomer().getName();
				String customer2 = o2.getTender() != null ? o2.getTender().getCustomer().getName()
						: o2.getCustomer().getName();
				return customer1.compareToIgnoreCase(customer2);
			};
		} else if (comparingParameter.equals("pick_up_date")) {
			comparator = (o1, o2) -> o1.getLoadingDate().compareTo(o2.getLoadingDate());
		} else if (comparingParameter.equals("route")) {
			comparator = (o1, o2) -> {
				String o1Start = getStartDescription(o1);
				String o2Start = getStartDescription(o2);
				int result = o1Start.compareTo(o2Start);
				return result != 0 ? result : getDestinationDescription(o1).compareTo(getDestinationDescription(o2));
			};
		} else throw new CommandException ("command is invoked with no arguments to sort transport orders");
		
		Set<TransportOrder>sortedOrders = new TreeSet<>(comparator);
		sortedOrders.addAll(orders);
		req.getSession().setAttribute("orders", sortedOrders);
		String uri = req.getRequestURI();
		req.setAttribute("orders", sortedOrders);
		try {
			req.getServletContext().getRequestDispatcher(uri).forward(req,resp);
		} catch (ServletException | IOException e) {
			logger.debug(e.getMessage());
			throw new CommandException("failure during executing of request forward back to the page",e);
		}
	}

	private String getStartDescription(TransportOrder order) {
		return order.getLine().getRoute().getStart().getLocality().getName()
				.concat(order.getLine().getRoute().getStart().getLocality().getCountry().getName());
	}

	private String getDestinationDescription(TransportOrder order) {
		return order.getLine().getRoute().getDestination().getLocality().getName()
				.concat(order.getLine().getRoute().getDestination().getLocality().getCountry().getName());
	}
}
