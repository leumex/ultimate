package by.minsk.training.command.validation;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public interface Validator {
	Map<String,String> validate(HttpServletRequest req);
}
