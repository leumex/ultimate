package by.minsk.training.command;

import by.minsk.training.ApplicationContext;
import by.minsk.training.core.Bean;
import by.minsk.training.security.AuthorizedException;
import by.minsk.training.security.SecurityService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static by.minsk.training.ApplicationConstants.LOGOUT_USER;

@Bean(name = LOGOUT_USER)
public class LogoutUserCommand implements ServletCommand {

    private static final Logger logger = LogManager.getLogger(LogoutUserCommand.class);

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        logger.debug("from within Logout command");
        SecurityService service = ApplicationContext.getInstance().getBean(SecurityService.class);
        try {
            service.logOut(req);
            resp.sendRedirect(req.getContextPath()+"/");
        } catch (IOException | AuthorizedException e) {
            logger.error(e.getMessage());
            throw new CommandException(e.getMessage(), e);
        }
    }
}
