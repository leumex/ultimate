package by.minsk.training.core;

public class BeanInstantionException extends BeanRegistrationException{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public BeanInstantionException(String s) {
        super(s);
    }
    public BeanInstantionException (String message, Exception e){
        super(message, e);
    }
}
