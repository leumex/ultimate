package by.minsk.training.core;

public class BeanRegistrationException extends RuntimeException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BeanRegistrationException(String message, Exception e) {
        super(message, e);
    }

    public BeanRegistrationException(String message) {
        super(message);
    }
}
