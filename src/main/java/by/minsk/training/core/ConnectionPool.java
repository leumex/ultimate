package by.minsk.training.core;

import java.sql.Connection;

public interface ConnectionPool {

    Connection getConnection();

    void close();
}
