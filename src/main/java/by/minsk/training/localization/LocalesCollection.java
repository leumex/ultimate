package by.minsk.training.localization;

import java.util.*;

public class LocalesCollection {

    private Map<String, Locale> localesMap = new HashMap<>();

    {
        localesMap.put("English", new Locale("en"));
        localesMap.put("German", new Locale("de", "DE"));
        localesMap.put("Dutch", new Locale("nl", "NL"));
        localesMap.put("Russian", new Locale("ru"));
    }

    public Locale get(String lang) {
        return localesMap.get(lang);
    }

    public int size() {
        return localesMap.size();
    }
}
