package by.minsk.training.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

import by.minsk.training.user.UserEntity;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Tender implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long id;
    private String title;
    private UserEntity customer;
    private LocalDate setDate;
    private LocalDate releaseDate;
    private LocalDate competitionDeadline;
    private LocalDate tenderStart;
    private LocalDate tenderEnd;
    private int estimatedQuantity;
    private State state = State.CREATED;

    public enum State {
        CREATED, RELEASED, COMPLETED, CONTRACTED, IN_ACTION
    }
}