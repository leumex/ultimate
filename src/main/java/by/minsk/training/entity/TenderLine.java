package by.minsk.training.entity;

import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TenderLine {
	private Long id;
	private Tender tender;
	private Line line;
	private Set<TenderLineQuery> lineQueries;
}
