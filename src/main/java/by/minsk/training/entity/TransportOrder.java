package by.minsk.training.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

import by.minsk.training.user.UserEntity;

@Data
@NoArgsConstructor
public class TransportOrder implements Serializable {
 
	private static final long serialVersionUID = 1L;
	private long id;
    private LocalDateTime issued;
    private LocalDate loadingDate;
    private int deliveryTerm;
    private Line line;
    private Tender tender;
    private UserEntity customer;
    private State state;
    private Set<TspPerformance> assignments;
    private SpotTransportOrder spot;

    public enum State {
        CREATED, ANNOUNCED, ASSIGNED, IN_ACTION, COMPLETED, WITHDRAWN, ACCEPTED
    }
}
