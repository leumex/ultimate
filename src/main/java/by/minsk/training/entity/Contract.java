package by.minsk.training.entity;

import by.minsk.training.user.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Contract implements Serializable {
   
	private static final long serialVersionUID = 2126031609346666954L;
	private long id;
    private UserEntity customer;
    private UserEntity tsp;
}
