package by.minsk.training.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class Country implements Serializable {
 
	private static final long serialVersionUID = 1L;
	private long id;
    private String name;
}
