package by.minsk.training.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SpotTransportOrder implements Serializable {
	 
	private static final long serialVersionUID = 1456631452332176156L;
	private Long id;
	private TransportOrder transportOrder;
	private LocalDateTime spotDeadline;
}
