package by.minsk.training.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class Route implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long id;
    private Conditions conditions = Conditions.FREE;
    private Address start;
    private Address destination;

    public enum Conditions {
        FREE("free"), OVER_UA_PROHIBITED("over Ukraine prohibited"),
        VIA_KOZLOVICHI_ONLY("via Kozlovichi only"), CUSTOMS_IN_BRYANSK("customs in Bryansk"),
        SANITARY_INSPECTION_IN_BENYAKONI("sanitary inspection in Benyakoni"),
        OVER_BY_PROHIBITED("through Belarus prohibited");

        String sql;

        Conditions(String sql) {
            this.sql = sql;
        }

        public String toString() {
            return this.sql;
        }

        public static Conditions getCondition(String sql) throws SQLException {
            Conditions definedValue;
            try {
                definedValue = Arrays.stream(Conditions.values()).filter(condition ->condition.toString().equals(sql))
                        .findFirst().orElseThrow(NoSuchElementException::new);
            } catch (NoSuchElementException e) {
                return null;
            }
            return definedValue;
        }

        public static List<String> getValues(){
            return Arrays.stream(Conditions.values()).map(Conditions::toString).collect(Collectors.toList());
        }
    }
}
