package by.minsk.training.entity;

import java.time.LocalDateTime;

import by.minsk.training.user.UserEntity;
import lombok.Data;

@Data
public class Bid {
	
	private Long id;
	private UserEntity tsp;
	private Double bid;
	private SpotTransportOrder spotOrder;
	private LocalDateTime madeAt;
}
