package by.minsk.training.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TenderLineQuery {
	private Long id;
	private Double rate;
	private Integer deliveryTerm;
	private Double awardedShare;
	private TenderParticipation participation;
	private TenderLine tenderLine;
}
