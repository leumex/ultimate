package by.minsk.training.entity;

import java.time.LocalDate;

import by.minsk.training.user.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TenderParticipation {
	private Long id;
	private LocalDate invitation;
	private LocalDate upToDateResponse;
	private Tender tender;
	private UserEntity tsp;
}
