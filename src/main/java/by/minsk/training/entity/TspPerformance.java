package by.minsk.training.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import by.minsk.training.user.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TspPerformance implements Serializable {
	 
	private static final long serialVersionUID = 4887415058845471728L;
	private Long shipment_id;
	private UserEntity tsp;
	private TransportOrder transportOrder;
	private TspResponse tspResponse;
	private LocalDateTime notified;
	private String truckPlates;
	private String comment;
	private LocalDateTime responseTime;
	private LocalDateTime arrivedForLoading;
	private LocalDateTime leftLoading;
	private LocalDateTime arrivedForUnloading;
	private LocalDateTime leftUnloading;
	
	
	public enum TspResponse {
		OUTSTANDING, ACCEPTED, WITHDRAWN, DECLINED
	}
}