package by.minsk.training.servlet;

import by.minsk.training.ApplicationContext;
import by.minsk.training.command.CommandException;
import by.minsk.training.command.ServletCommand;
import by.minsk.training.service.ServiceException;
import by.minsk.training.service.notification.NotificationScanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/", loadOnStartup = 1, name = "app")
public class ApplicationServlet extends HttpServlet {

    private static final long serialVersionUID = 33452345852384930L;
    private static final Logger logger = LogManager.getLogger(ApplicationServlet.class);
    private static final ApplicationContext CONTEXT = ApplicationContext.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String commandName = req.getParameter("commandName");
        logger.debug(commandName + " command has been invoked");
        ServletCommand command = CONTEXT.getBean(commandName);
        if (command != null) {
            try {
                command.execute(req, resp);
                logger.debug(commandName + " command has been executed completely");
            } catch (CommandException e) {
                throw new ServletException(e);
            }
        } else {
        	NotificationScanner notificationScanner = ApplicationContext.getInstance().getBean(NotificationScanner.class);
        	try {
				notificationScanner.collectNotifications(req);
			} catch (ServiceException e) {
				throw new ServletException(e.getMessage(), e);
			}
            finally {req.getRequestDispatcher("/jsp/layout.jsp").forward(req, resp);
        }
      }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
