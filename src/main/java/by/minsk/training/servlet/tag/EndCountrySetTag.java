package by.minsk.training.servlet.tag;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import by.minsk.training.entity.Line;

public class EndCountrySetTag {

/*
    private static final LineDAO lineDAO = ApplicationContext.getInstance().getBean(LineDAOImpl.class);
*/

    public static Set<String> retrieveSet(Collection<Line> lines) {
        return /*lines.stream().map(line -> {
            try {
                return lineDAO.getEndLocation(line.getId());
            } catch (SQLException e) {
                return null;
            }
        }).collect(Collectors.toSet());
    }*/ new HashSet<String>();
    }
}
