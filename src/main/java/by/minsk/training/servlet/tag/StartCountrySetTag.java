package by.minsk.training.servlet.tag;

import by.minsk.training.entity.Line;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class StartCountrySetTag {

    public static Set<String> retrieveSet (Collection<Line> lines){
        return /*lines.stream().map(line -> line.getRouteLine().getStart().getLocality().getCountry().getName()).
                collect(Collectors.toSet());*/new HashSet<String>();
    }
}
