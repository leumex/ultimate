package by.minsk.training.servlet.tag;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class SetConvertionTag {
public static Set<String> convert (Collection<String> list){
    return new HashSet<>(list);
}
}
