package by.minsk.training.service;

import by.minsk.training.entity.TransportOrder;
import by.minsk.training.entity.TspPerformance;

public interface TspPerformanceService {
	/*
	 * retrieves all tsp_response records from MySQL DB and binds them to
	 * correspondent transport order
	 */	
	TransportOrder bindAssignments(TransportOrder transportOrder) throws ServiceException;
}