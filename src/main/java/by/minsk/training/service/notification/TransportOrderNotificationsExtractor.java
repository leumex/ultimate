package by.minsk.training.service.notification;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import by.minsk.training.core.Bean;
import by.minsk.training.entity.Bid;
import by.minsk.training.entity.SpotTransportOrder;
import by.minsk.training.entity.TransportOrder;
import by.minsk.training.entity.TspPerformance;
import by.minsk.training.service.BidsService;
import by.minsk.training.user.UserEntity;
import lombok.AllArgsConstructor;

@Bean
@AllArgsConstructor
public class TransportOrderNotificationsExtractor {

	private BidsService bidsService;

	private static List<TspPerformance.TspResponse> responses = new ArrayList<>(2);
	static {
		responses.add(TspPerformance.TspResponse.OUTSTANDING);
		responses.add(TspPerformance.TspResponse.ACCEPTED);
	}

	public Set<UserNotification> extractBasicNotifications(UserEntity entity, TransportOrder order,
			TspPerformance assignment) {
		UserEntity.Roles role = entity.getRole();
		Set<UserNotification> notifications = new HashSet<>();
		if (role == UserEntity.Roles.CUSTOMER) {
			notifications.add(checkOrderDeclining(order, assignment));
			notifications.add(checkAssignmentResponse(order, assignment));
			notifications.add(checkTruckPlates(order, assignment));
			notifications.add(checkArrivalForLoading(order, assignment));
		} else if (role == UserEntity.Roles.TRANSPORT_SERVICE_PROVIDER) {
			notifications.add(checkOrderRecall(order, assignment));
			notifications.add(checkNewOrderEmergence(order, assignment));
			notifications.add(checkTrackingEvents(order, assignment));
		}
		return notifications.stream().filter(n -> n != null).collect(Collectors.toSet());
	}

	public Set<UserNotification> extractSpotNotifications(TransportOrder order, SpotTransportOrder spotOrder,
			UserEntity entity) {
		Set<UserNotification> notifications = new HashSet<>();
		if (entity.getRole() == UserEntity.Roles.CUSTOMER) {
			notifications.add(checkSpotOrderDeadline(order, spotOrder));
		} else if (entity.getRole() == UserEntity.Roles.TRANSPORT_SERVICE_PROVIDER) {
			notifications.add(checkNewSpotOrder(order, spotOrder, entity));
		}
		return notifications.stream().filter(n -> n != null).collect(Collectors.toSet());
	}

	private UserNotification checkOrderDeclining(TransportOrder order, TspPerformance assignment) {
		return (order.getState() == TransportOrder.State.ASSIGNED
				& assignment.getTspResponse() == TspPerformance.TspResponse.DECLINED)
						? new UserNotification(order.getCustomer().getId(), Alert.TO_REJECTION, order.getId(),
								"Transport order has been declined by TSP")
						: null;
	}

	private UserNotification checkOrderRecall(TransportOrder order, TspPerformance assignment) {
		return (order.getState() == TransportOrder.State.WITHDRAWN & responses.contains(assignment.getTspResponse()))
				? new UserNotification(assignment.getTsp().getId(), Alert.TO_RECALL, order.getId(),
						assignment.getComment())
				: null;
	}

	private UserNotification checkAssignmentResponse(TransportOrder order, TspPerformance assignment) {
		LocalDateTime presentTime = LocalDateTime.now();
		LocalDateTime assigmentTime = assignment.getNotified();
		Duration duration = Duration.between(assigmentTime, presentTime);
		Long seconds = duration.getSeconds();
		Long hours = seconds / 3600;
		return (order.getState() == TransportOrder.State.ASSIGNED
				& assignment.getTspResponse() == TspPerformance.TspResponse.OUTSTANDING & hours >= 2)
						? new UserNotification(order.getCustomer().getId(), Alert.TO_ASSIGNMENT_RESPONSE, order.getId(),
								"2 hours have passed since the order has been assigned to the TSP")
						: null;
	}

	private UserNotification checkTruckPlates(TransportOrder order, TspPerformance assignment) {
		LocalDate loadingDate = order.getLoadingDate();
		LocalDateTime loadingTime = LocalDateTime.of(loadingDate, LocalTime.of(07, 00));
		LocalDateTime presentTime = LocalDateTime.now();
		Duration duration = Duration.between(presentTime, loadingTime);
		long hours = duration.getSeconds() / 3600;
		String truckPlates = assignment.getTruckPlates();
		return (order.getState() == TransportOrder.State.ACCEPTED
				& assignment.getTspResponse() == TspPerformance.TspResponse.ACCEPTED & hours <= 12
				& (truckPlates == null | truckPlates.length() < 8))
						? new UserNotification(order.getCustomer().getId(), Alert.TRUCK_PLATES, order.getId(),
								"Truck plates are missing")
						: null;
	}

	private UserNotification checkArrivalForLoading(TransportOrder order, TspPerformance assignment) {
		LocalDate loadingDate = order.getLoadingDate();
		LocalDateTime loadingTime = LocalDateTime.of(loadingDate, LocalTime.of(07, 00));
		LocalDateTime presentTime = LocalDateTime.now();
		Duration duration = Duration.between(loadingTime, presentTime);
		long hours = duration.getSeconds() / 3600;
		LocalDateTime arrival = assignment.getArrivedForLoading();
		return (order.getState() == TransportOrder.State.ACCEPTED
				& assignment.getTspResponse() == TspPerformance.TspResponse.ACCEPTED & hours >= 72 & arrival == null)
						? new UserNotification(order.getCustomer().getId(), Alert.ARRIVAL_FOR_LOADNING, order.getId(),
								"Truck arrival for loading has not been registered so far")
						: null;
	}

	private UserNotification checkNewOrderEmergence(TransportOrder order, TspPerformance assignment) {
		return (order.getState() == TransportOrder.State.ASSIGNED
				& assignment.getTspResponse() == TspPerformance.TspResponse.OUTSTANDING)
						? new UserNotification(assignment.getTsp().getId(), Alert.NEW_TO, order.getId(),
								"New order has been assigned to you")
						: null;
	}

	private UserNotification checkTrackingEvents(TransportOrder order, TspPerformance assignment) {
		boolean leftLoadingEventAbsence = assignment.getLeftLoading() == null;
		LocalDate loadingDate = order.getLoadingDate();
		LocalDateTime loadingTime = LocalDateTime.of(loadingDate, LocalTime.of(07, 00));
		LocalDateTime presentTime = LocalDateTime.now();
		Duration duration = Duration.between(loadingTime, presentTime);
		long hours = duration.getSeconds() / 3600;
		boolean leftLoading72hrsAgo = hours >= 72;
		if (leftLoadingEventAbsence & leftLoading72hrsAgo) {
			return new UserNotification(assignment.getTsp().getId(), Alert.TO_TRACKING, order.getId(),
					"Dispatch from loading point record is missing");
		} else {
			boolean arrivedUnloadingEventAbsence = assignment.getArrivedForUnloading() == null;
			LocalDate shouldHaveDelivered = loadingDate.plusDays(order.getDeliveryTerm() + 5);
			LocalDateTime scheduledDeliveyTime = LocalDateTime.of(shouldHaveDelivered, LocalTime.of(07, 00));
			duration = Duration.between(scheduledDeliveyTime, presentTime);
			hours = duration.getSeconds() / 3600;
			boolean shouldHaveDelivered72hrsAgo = hours >= 72;
			if (arrivedUnloadingEventAbsence & shouldHaveDelivered72hrsAgo) {
				return new UserNotification(assignment.getTsp().getId(), Alert.TO_TRACKING, order.getId(),
						"Arrival at destination record is missing");
			} else {
				boolean leftUnloadingEventAbsence = assignment.getLeftUnloading() == null;
				return (leftUnloadingEventAbsence & shouldHaveDelivered72hrsAgo)
						? new UserNotification(assignment.getTsp().getId(), Alert.TO_TRACKING, order.getId(),
								"Completed delivery at destination record is missing")
						: null;
			}
		}
	}

	private UserNotification checkSpotOrderDeadline(TransportOrder order, SpotTransportOrder spotOrder) {
		return (order.getState() == TransportOrder.State.ANNOUNCED
				& spotOrder.getSpotDeadline().isAfter(LocalDateTime.now()))
						? new UserNotification(order.getCustomer().getId(), Alert.SPOT_DEADLINE, order.getId(),
								"Spot contest is over, time to assign the order")
						: null;
	}

	private UserNotification checkNewSpotOrder(TransportOrder order, SpotTransportOrder spotOrder, UserEntity tsp) {
		Bid bid = bidsService.retrieveBid(spotOrder, tsp);
		return (order.getState() == TransportOrder.State.ANNOUNCED & bid == null)
				? new UserNotification(tsp.getId(), Alert.NEW_SPOT_TO, order.getId(), "Check new spot order, please")
				: null;
	}
}
