package by.minsk.training.service.notification;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserNotification {
	private Long targetUser;
	private Alert type;
	private Long referencedObject;
	private String description;
}
