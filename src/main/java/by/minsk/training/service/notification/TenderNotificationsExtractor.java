package by.minsk.training.service.notification;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import by.minsk.training.core.Bean;
import by.minsk.training.entity.Tender;
import by.minsk.training.entity.TenderLine;
import by.minsk.training.entity.TenderLineQuery;
import by.minsk.training.entity.TenderParticipation;
import by.minsk.training.service.TenderService;
import by.minsk.training.user.UserEntity;
import lombok.AllArgsConstructor;

@Bean
@AllArgsConstructor
public class TenderNotificationsExtractor {
	
	private TenderService tenderService;
	
	
	private UserNotification checkNewTenderInvitation (TenderParticipation participation) {
		Set<TenderLineQuery> lineQueries  = tenderService.getQueries(participation);
		return (lineQueries==null||lineQueries.isEmpty())?
				new UserNotification(participation.getTsp().getId(), Alert.NEW_TENDER, participation.getTender().getId(),"new tender has been recently opened for quotation")
				:null;
	}
	
	private UserNotification checkTenderDeadline (Tender tender) {
		Set<TenderLine> lines = tenderService.getTenderLines(tender);
		return lines.stream().peek(line ->tenderService.bindLineQueries(line, tender)).anyMatch(line -> tenderService.calculateAssignedLineVolume(line)<100)
		? new UserNotification (tender.getCustomer().getId(),Alert.TENDER_COMPETITION_DEADLINE, tender.getId(), "there's unassigned volume of some tender line(s)")
				:null;
	}
	
	public Set<UserNotification> extractTenderNotifications (UserEntity entity, Tender tender){
		Set<UserNotification> notifications = new HashSet<>();
		UserEntity.Roles role = entity.getRole();
		if (role == UserEntity.Roles.CUSTOMER) {
			notifications.add(checkTenderDeadline(tender));
		} else if (role == UserEntity.Roles.TRANSPORT_SERVICE_PROVIDER) {
			notifications.add(checkNewTenderInvitation(tenderService.getTenderParticipation(tender, entity)));
		}
		return notifications.stream().filter(n -> n!=null).collect(Collectors.toSet());
	}

}
