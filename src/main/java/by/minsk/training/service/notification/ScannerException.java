package by.minsk.training.service.notification;

public class ScannerException extends Exception {
	
	private static final long serialVersionUID = 3176236972495841429L;

	public ScannerException() {
	};

	public ScannerException(String message, Throwable exception) {
		super(message, exception);
	}

	public ScannerException(Throwable exception) {
		super(exception);
	}

}
