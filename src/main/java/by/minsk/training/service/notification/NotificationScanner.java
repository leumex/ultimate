package by.minsk.training.service.notification;

import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.minsk.training.core.Bean;
import by.minsk.training.entity.SpotTransportOrder;
import by.minsk.training.entity.Tender;
import by.minsk.training.entity.TransportOrder;
import by.minsk.training.entity.TspPerformance;
import by.minsk.training.service.ServiceException;
import by.minsk.training.service.SpotOrderService;
import by.minsk.training.service.TenderService;
import by.minsk.training.service.TransportOrderService;
import by.minsk.training.service.TspPerformanceService;
import by.minsk.training.user.UserEntity;
import lombok.AllArgsConstructor;

@Bean
@AllArgsConstructor
public class NotificationScanner {

	private TransportOrderService transportOrderService;
	private TspPerformanceService tspPerformanceService;
	private TransportOrderNotificationsExtractor fromOrdersExtractor;
	private TenderNotificationsExtractor fromTendersExtractor;
	private TenderService tenderService;
	private SpotOrderService spotOrderService;
	
	private static Set<UserNotification> notifications;
	
	public static Set<UserNotification> getNotifications() {
		return notifications;
	}

	public void collectNotifications(HttpServletRequest req) throws ServiceException {
		HttpSession session = req.getSession();
		UserEntity user = (UserEntity) session.getAttribute("user");
		if (user == null) {
			notifications = null;
			session.setAttribute("notifications", null);
			return;
		}
		Set<TransportOrder> incompleteOrders = transportOrderService.detectIncompleteOrders(user);
		incompleteOrders = incompleteOrders.stream()
				.map(HandlingExceptionFunction.check(order -> tspPerformanceService.bindAssignments(order)))
				.collect(Collectors.toSet());
		notifications = incompleteOrders.stream().flatMap(order -> this.inspectTransportOrder(order, user).stream()).collect(Collectors.toSet());
		Set<Tender> tenders = tenderService.detectNewReleasedTenders(user);
		notifications.addAll(tenders.stream().flatMap(tender -> fromTendersExtractor.extractTenderNotifications(user, tender).stream()).filter(n -> n!=null).collect(Collectors.toSet()));
		session.setAttribute("notifications", notifications);
	}
	
	private Set<UserNotification> inspectTransportOrder (TransportOrder order, UserEntity entity){
		 Set<UserNotification> notifications;
		 Set<TspPerformance> assignments = order.getAssignments();
		 notifications = assignments.stream().flatMap(assignment -> fromOrdersExtractor.extractBasicNotifications(entity, order, assignment).stream()).collect(Collectors.toSet());
		 SpotTransportOrder spotOrder = spotOrderService.getSpotOrder(order);
		 if(spotOrder!=null) {notifications.addAll(fromOrdersExtractor.extractSpotNotifications(order, spotOrder, entity));}
		 return notifications;
	}	
}
