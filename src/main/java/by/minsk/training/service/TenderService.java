package by.minsk.training.service;

import java.util.Set;

import by.minsk.training.entity.Tender;
import by.minsk.training.entity.TenderLine;
import by.minsk.training.entity.TenderLineQuery;
import by.minsk.training.entity.TenderParticipation;
import by.minsk.training.user.UserEntity;

public interface TenderService {

	Set<TenderLineQuery> getQueries (TenderParticipation participation);
	Set<TenderLine> getTenderLines (Tender tender);
	void bindLineQueries(TenderLine line, Tender tender);
	int calculateAssignedLineVolume (TenderLine line);
	TenderParticipation getTenderParticipation (Tender tender, UserEntity entity);
	Set<Tender> detectNewReleasedTenders(UserEntity entity);
}
