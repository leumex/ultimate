package by.minsk.training.service;

import java.util.Set;

import by.minsk.training.entity.TransportOrder;
import by.minsk.training.entity.TspPerformance;
import by.minsk.training.user.UserEntity;

public interface TransportOrderService {
	/*
	 * retrieves all transport orders with states all but 'COMPLETED' bound to the
	 * UserEntity user
	 */
	Set<TransportOrder> detectIncompleteOrders(UserEntity user) throws ServiceException;
	Set<TransportOrder> gatherOrders(UserEntity user) throws ServiceException;
	TspPerformance getLatestAssignment(TransportOrder transportOrder) throws ServiceException;
}
