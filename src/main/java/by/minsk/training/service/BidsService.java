package by.minsk.training.service;

import by.minsk.training.entity.Bid;
import by.minsk.training.entity.SpotTransportOrder;
import by.minsk.training.user.UserEntity;

public interface BidsService {
Bid retrieveBid (SpotTransportOrder order, UserEntity tsp);
}
