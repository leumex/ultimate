package by.minsk.training.service;

import by.minsk.training.entity.SpotTransportOrder;
import by.minsk.training.entity.TransportOrder;

public interface SpotOrderService {
	SpotTransportOrder getSpotOrder (TransportOrder order);
}
