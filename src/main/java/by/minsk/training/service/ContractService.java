package by.minsk.training.service;

import by.minsk.training.entity.Contract;
import by.minsk.training.user.UserEntity;

public interface ContractService {

	Contract defineContract(UserEntity customer, UserEntity tsp);
}
