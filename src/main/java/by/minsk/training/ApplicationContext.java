package by.minsk.training;

import by.minsk.training.command.*;
import by.minsk.training.command.pagination.TableDemostrator;
import by.minsk.training.command.validation.RegValidator;
import by.minsk.training.core.BeanRegistry;
import by.minsk.training.core.BeanRegistryImpl;
import by.minsk.training.dao.*;
import by.minsk.training.dao.basic.*;
import by.minsk.training.localization.BundleProvider;
import by.minsk.training.security.SecurityService;
import by.minsk.training.service.notification.NotificationScanner;
import by.minsk.training.service.notification.TenderNotificationsExtractor;
import by.minsk.training.service.notification.TransportOrderNotificationsExtractor;
import by.minsk.training.user.UserDaoImpl;
import by.minsk.training.user.UserServiceImpl;
/*import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
*/
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ApplicationContext implements BeanRegistry {
    private final static AtomicBoolean INITIALIZED = new AtomicBoolean(false);
    private final static Lock INITIALIZE_LOCK = new ReentrantLock();
	/*
	 * private static final Logger logger =
	 * LogManager.getLogger(ApplicationContext.class);
	 */
    private static ApplicationContext INSTANCE;

    private BeanRegistry beanRegistry = new BeanRegistryImpl();

    private ApplicationContext() {
    }

    public static void initialize() {
        INITIALIZE_LOCK.lock();
        try {
            if (INSTANCE != null && INITIALIZED.get()) {
                throw new IllegalStateException("Context has been already initialized");
            } else {
                ApplicationContext context = new ApplicationContext();
                context.init();
                INSTANCE = context;
                INITIALIZED.set(true);
            }
        } finally {
            INITIALIZE_LOCK.unlock();
        }
    }

    public static ApplicationContext getInstance() {
        if (INSTANCE != null && INITIALIZED.get()) {
            return INSTANCE;
        } else {
            throw new IllegalStateException("Context has not been yet initialized!");
        }
    }

    @Override
    public <T> void registerBean(T bean) {
        this.beanRegistry.registerBean(bean);
    }

    @Override
    public <T> void registerBean(Class<T> beanClass) {
        this.beanRegistry.registerBean(beanClass);
    }

    @Override
    public <T> T getBean(Class<T> beanClass) {
        return this.beanRegistry.getBean(beanClass);
    }

    @Override
    public <T> T getBean(String name) {
        return this.beanRegistry.getBean(name);
    }

    @Override
    public <T> boolean removeBean(T bean) {
        return this.beanRegistry.removeBean(bean);
    }

    @Override
    public void destroy() {
        DataSource dataSource = this.getBean(DataSource.class);
        dataSource.close();
        beanRegistry.destroy();
        INITIALIZED.set(false);
    }

    private void init() {
        registerDataSource();
        registerClasses();
    }

    private void registerDataSource() {
        DataSource dataSource = DataSourceImpl.getInstance();
        TransactionManager transactionManager = new TransactionManagerImpl(dataSource);
        ConnectionManager connectionManager = new ConnectionManagerImpl(transactionManager, dataSource);
        TransactionInterceptor transactionInterceptor = new TransactionInterceptor(transactionManager);
        registerBean(dataSource);
        registerBean(transactionManager);
        registerBean(connectionManager);
        registerBean(transactionInterceptor);
    }

    private void registerClasses() {
        registerBean(UserDaoImpl.class);
        registerBean(UserServiceImpl.class);
        registerBean(RegValidator.class);
        registerBean(RegisterUserSaveCommand.class);
        registerBean(LoginUserCommand.class);
        registerBean(SecurityService.class);
        registerBean(LogoutUserCommand.class);
        registerBean(BundleProvider.class);
        registerBean(NotificationScanner.class);
        registerBean(TenderNotificationsExtractor.class);
        registerBean(TransportOrderNotificationsExtractor.class);
        registerBean(CountryDAOImpl.class);
        registerBean(AddressDAOImpl.class);
        registerBean(LocalityDAOImpl.class);
        registerBean(RouteDAOImpl.class);
        registerBean(LineDAOImpl.class);
        registerBean(TruckloadDAOImpl.class);
        registerBean(TransportOrderDAOImpl.class);
        registerBean(TenderDAOImpl.class);
        registerBean(TableDemostrator.class);
    }
}
